package application.controller;


import application.database.model.PostImpl;
import application.database.model.UserImpl;
import application.database.services.post.PostServiceImpl;
import application.database.services.user.UserServiceImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/controller")
@CrossOrigin
public class UserController {

    @Autowired
    private UserServiceImpl userService;


    @Autowired
    private PostServiceImpl postService;


    private JSONObject createEmployeeJSON(UserImpl user) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", user.getId());
        jsonObject.put("firstName", user.getFirstName());
        jsonObject.put("lastName", user.getLastName());
        jsonObject.put("lastName", user.getEmail());
        return jsonObject;
    }

    @RequestMapping(value = "/user/findById", method = RequestMethod.GET)
    public String getEmployeeById(@RequestParam Integer id) {
        UserImpl user = userService.getUserById(id);
        if (user == null)
        {
            return "No user with such id";
        }
        return createEmployeeJSON(user).toString();
    }

    private JSONObject createPostJSON(PostImpl post) {
        JSONObject resultJson = new JSONObject();
        resultJson.put("id", post.getPostId());
        resultJson.put("post_text", post.getPostText());
        return resultJson;
    }

    private JSONObject createJSONResponseForPosts(List<PostImpl> posts) {
        JSONObject result = new JSONObject();
        JSONArray arr = new JSONArray();
        for (PostImpl post : posts) {
            arr.put(createPostJSON(post));
        }
        return result.put("posts", arr);
    }

    @RequestMapping(value = "/post/findById", method = RequestMethod.GET)
    public String getPostById(@RequestParam Integer id) {
        PostImpl post = postService.getPostById(id);
        if (post == null)
        {
            return "No posts with such id";
        }
        return createPostJSON(post).toString();
    }

}
