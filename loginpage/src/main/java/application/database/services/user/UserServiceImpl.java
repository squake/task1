package application.database.services.user;

import application.database.model.PostImpl;
import application.database.model.UserImpl;
import application.database.repositories.PostRepository;
import application.database.repositories.UserRepository;
import application.database.services.post.PostService;
import application.database.services.post.PostServiceImpl;
import application.database.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    //why not @Autowired?
    private UserRepository userRepository;
    private PostServiceImpl postService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PostServiceImpl postService) {
        this.userRepository = userRepository;
        this.postService = postService;
    }

    @Override
    public UserImpl getUserById(int id) {
        return userRepository.findOne(id);
    }

    @Override
    public void saveUser(UserImpl user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(int id) {
        UserImpl user = userRepository.findOne(id);
        user.getPosts().forEach(post -> postService.deletePostById(post.getPostId()));
        userRepository.delete(id);
    }

    @Override
    public void updateUserById(int id) {

    }

    @Override
    public boolean validateUser(UserImpl user) {
        return userRepository.exists(user.getId());
    }

    @Override
    public List<UserImpl> getAllUsers() {

        List<UserImpl> users = new ArrayList<>();
        this.userRepository.findAll().forEach(users::add);
        return users;

    }
}
