package application.database.services.user;

import application.database.model.UserImpl;

import java.util.List;

public interface UserService {

    UserImpl getUserById(int id);
    void saveUser(UserImpl user);
    void deleteUserById(int id);
    void updateUserById(int id);
    boolean validateUser(UserImpl user);
    List<UserImpl> getAllUsers();
}
