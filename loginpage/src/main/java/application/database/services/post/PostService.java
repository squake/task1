package application.database.services.post;

import application.database.model.PostImpl;

import java.util.List;

public interface PostService {

    PostImpl getPostById(int id);
    void savePost(PostImpl post);
    void deletePostById(int id);
    void updatePostById(int id);
    List<PostImpl> getAllPosts();

}
