package application.database.services.post;

import application.database.model.PostImpl;
import application.database.repositories.PostRepository;
import application.database.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostServiceImpl implements PostService{

    PostRepository postRepository;

    @Autowired
    PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public PostImpl getPostById(int id) {
        return postRepository.findOne(id);
    }

    @Override
    public void savePost(PostImpl post) {
        postRepository.save(post);
    }

    @Override
    public void deletePostById(int id) {
        postRepository.delete(id);
    }

    @Override
    public void updatePostById(int id) {

    }

    @Override
    public List<PostImpl> getAllPosts() {
        List<PostImpl> posts = new ArrayList<>();
        this.postRepository.findAll().forEach(posts::add);
        return posts;
    }
}
