package application.database.repositories;

import application.database.model.UserImpl;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserImpl, Integer> {
}
