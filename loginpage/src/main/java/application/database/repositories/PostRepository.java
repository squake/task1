package application.database.repositories;

import application.database.model.PostImpl;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<PostImpl, Integer> {
}
