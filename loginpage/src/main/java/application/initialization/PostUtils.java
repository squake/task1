package application.initialization;

import application.database.model.PostImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostUtils {

    public static List<PostImpl> createPosts() {
        return new ArrayList<PostImpl>() {{
            add(new PostImpl("Hello world!"));
            add(new PostImpl("Such a perfect day :)"));
            add(new PostImpl("I should have done my homework yesterday. But let me tell you what happened"));
        }};
    }
}
