package application.initialization;

import application.database.model.PostImpl;
import application.database.model.UserImpl;
import application.database.services.post.PostServiceImpl;
import application.database.services.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class initDatabases implements ApplicationRunner {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    PostServiceImpl postService;

    private void addUsersInDataBase() {
        List<UserImpl> users = UserUtils.createUsers();
        users.forEach(user -> userService.saveUser(user));
    }

    private void addPostsInDataBase() {
        List<PostImpl> posts = PostUtils.createPosts();
        posts.forEach(post -> postService.savePost(post));
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        addUsersInDataBase();
        addPostsInDataBase();


        List<UserImpl> users = userService.getAllUsers();
        List<PostImpl> posts = postService.getAllPosts();

        posts.get(0).setUser(users.get(0));
        posts.get(1).setUser(users.get(0));
        posts.get(2).setUser(users.get(1));

        userService.saveUser(users.get(0));
        userService.saveUser(users.get(1));
        userService.saveUser(users.get(2));
        userService.saveUser(users.get(3));

        postService.savePost(posts.get(0));
        postService.savePost(posts.get(1));
        postService.savePost(posts.get(2));

    }
}
