package application.initialization;

import application.database.model.UserImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class UserUtils {

    protected static List<UserImpl> createUsers() {
        return new ArrayList<UserImpl>(){{
            add(new UserImpl("Maria", "Evans", "maria@gmail.com"));
            add(new UserImpl("Alice", "White","alicewww@outlook.com"));
            add(new UserImpl("Kenny", "Snow","kkkennny@gmail.com"));
            add(new UserImpl("Leeroy", "Jenkins","jenk@gmail.com"));
        }};
    }
}
