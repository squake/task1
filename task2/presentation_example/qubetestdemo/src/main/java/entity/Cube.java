package entity;

public class Cube {
    private int x, y, z;

    public double volume() {
        double volume = x * y * z;
        if (volume >= 0)
            return volume;
        else
            throw new IllegalStateException();
    }

    public double wrongVolume() {
        return x * y;
    }

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }
}
