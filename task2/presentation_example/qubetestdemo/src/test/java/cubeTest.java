import entity.Cube;
import org.junit.*;

public class cubeTest {

    Cube cube = new Cube(3,3,3);

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before class!");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After class!");
    }

    @Before
    public void beforeTest() {
        System.out.println("Before test!");
    }

    @After
    public void afterTest() {
        System.out.println("After test!");
    }

    @Test
    public void volume() {
        System.out.println("Testing volume()!");
        Assert.assertEquals(3*3*3,cube.volume(),0);
    }

    @Test
    public void wrongVolume() {
        System.out.println("Testing wrong volume()!");
        Assert.assertEquals(3*3*3, cube.wrongVolume(),0);
    }


    @Test(expected = IllegalStateException.class)
    public void negative() {
        System.out.println("Testing negative()!");
        cube.setX(-3);
        cube.volume();
    }

    @Test(expected = IllegalStateException.class)
    public void wrongNegative() {
        System.out.println("Testing wrong wrongNegative()!");
        cube.setX(-3);
        cube.wrongVolume();
    }
}